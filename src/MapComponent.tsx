import * as React from 'react';
import Collection from 'ol/Collection';
import { initMap } from './modules/initMap';

type MapComponentState = {
  mapDomRef: any;
  features: any;
  isAllowToDraw: null | any;
  layer: any;
  featureCoordinates: number[];
  mouseCoordinate: number[] | null;
};

class MapComponent extends React.Component<{}, MapComponentState> {
  componentDidMount() {
    const layers: any = new Collection();
    initMap(layers, this.state.mapDomRef.current);
  }

  toggleToolsDraw = (status: null | any) => {
    this.setState({
      isAllowToDraw: status,
    });
  };

  render() {
    return (
      <div className="App">
        <div className={`map ${this.state.isAllowToDraw && 'crosshair'}`} ref={this.state.mapDomRef}></div>

        <div className="mt-2">
          <button
            className={`btn btn-primary mr-2 ${this.state.isAllowToDraw && 'btn-primary-active'}`}
            onClick={() => this.toggleToolsDraw(true)}
          >
            MEASURETOOLS
          </button>

          <button
            className={`btn btn-primary mr-2 ${this.state.isAllowToDraw || 'btn-primary-active'}`}
            onClick={() => this.toggleToolsDraw(false)}
          >
            STOP
          </button>
        </div>
      </div>
    );
  }
}

export default MapComponent;
